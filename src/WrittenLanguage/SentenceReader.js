/**
 * Highly experimental stuff still, and highly WIP.
 *
 * Finish essentials first before continuing with this.
 *
 * */

import nlp from "compromise";

export const normalizeClause = clause =>
  nlp(clause)
    .normalize()
    .out();

const clauseToNegative = clause =>
  nlp(clause)
    .sentences()
    .toNegative()
    .out();

export const getTruthValue = proposition => {
  const normalized = normalizeClause(proposition);
  const negation = clauseToNegative(normalized);
  return normalized !== negation;
};

export const isImplication = input => {
  return (
    input.toLowerCase().includes("if ") &&
    input.toLowerCase().includes(" then ")
  );
};

export const getClausesFromImplication = input => {
  const inputAsArray = input.split(" ").filter(value => value !== "");
  const indexOfThen = inputAsArray.indexOf("then");
  return [
    inputAsArray.slice(1, indexOfThen).join(" "),
    inputAsArray.slice(indexOfThen + 1).join(" ")
  ];
};

export const isNegation = proposition => !getTruthValue(proposition);

export const hasConjunctions = input => {
  const inputAsArray = input.split(" ").filter(value => value !== "");
  return (
    input.toLowerCase().includes(" and ") &&
    inputAsArray.length >= 3 &&
    inputAsArray.indexOf("and") !== 0 &&
    inputAsArray.indexOf("and") !== inputAsArray.length - 1
  );
};

export const getPropositionsOfConjunction = input => {
  const inputAsArray = input.split(" ").filter(value => value !== "");
  const indexOfConjunction = inputAsArray.indexOf("and");
  return [
    inputAsArray.slice(0, indexOfConjunction).join(" "),
    inputAsArray.slice(indexOfConjunction + 1).join(" ")
  ];
};

export const getTruthValueForInput = input => {
  if (isImplication(input)) {
    const clauses = getClausesFromImplication(input);
    if (hasConjunctions(input)) {
      const propositions = getPropositionsOfConjunction(input);
    }
  }
};
