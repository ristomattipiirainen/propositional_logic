const NEGATION = "!",
  CONJUNCTION = "AND",
  DISJUNCTION = "OR",
  IMPLICATION = "->",
  EQUIVALENCE = "<=>",
  XOR = "XoR";

const CONNECTORS = [CONJUNCTION, DISJUNCTION, IMPLICATION, EQUIVALENCE, XOR];
const PREDENCE_ORDERED_LOGICAL_OPERATORS = [NEGATION, ...CONNECTORS];

export const getNumOfConnectors = sentence => {
  let numOfConnectors = 0;

  CONNECTORS.forEach(connector => {
    const re = new RegExp(connector, "g") || [];
    numOfConnectors += sentence.match(re) ? sentence.match(re).length : 0;
  });

  return numOfConnectors;
};

export const getAllConnectors = sentence => {
  return sentence
    .split(" ")
    .filter(word => CONNECTORS.some(connector => connector === word));
};

export const hasRemainingDisjunctions = (connectors, index) => {
  if (index >= connectors.length) {
    return false;
  }
  return connectors.slice(index).some(connector => connector === DISJUNCTION);
};

const shouldAddConnectorWithOpeningParenthesis = (
  word,
  numOfConnectors,
  numOfConnectorsHandled
) => {
  return (
    (word === CONJUNCTION || word === DISJUNCTION) &&
    numOfConnectorsHandled < numOfConnectors
  );
};

const addBracketsToConjunction = (newSentence, index, word) => {
  newSentence.splice(index + 1, 1); // remove already added opening parenthesis
  newSentence.push(word);
  newSentence.splice(index - 1, 0, "("); // add opening parenthesis before conj.
  newSentence.push(")");

  return newSentence;
};

/**
 * Logic is applied from here:
 *
 * https://en.wikipedia.org/wiki/Logical_connective#Order_of_precedence
 *
 */

export const addParenthesis = sentence => {
  let newSentence = [];
  let parenthesisToClose = 0;
  let numOfConnectorsHandled = 0;

  const numOfConnectors = getNumOfConnectors(sentence);
  const allConnectors = getAllConnectors(sentence);
  const words = sentence.split(" ");

  if (numOfConnectors <= 1) {
    return `(${sentence})`;
  }

  PREDENCE_ORDERED_LOGICAL_OPERATORS.forEach(connector => {});

  // words.forEach((word, index) => {
  //   if (CONNECTORS.some(connector => connector === word)) {
  //     numOfConnectorsHandled++;
  //   }
  //   if (
  //     shouldAddConnectorWithOpeningParenthesis(
  //       word,
  //       numOfConnectors,
  //       numOfConnectorsHandled
  //     )
  //   ) {
  //     newSentence.push(...[word, "("]);
  //     parenthesisToClose++;
  //   } else {
  //     newSentence.push(word);
  //   }
  // });

  // final parenthesises
  const closingParenthesis = ")".repeat(parenthesisToClose);
  return `(${formStringFromArray(newSentence)}${closingParenthesis})`;
};

export const formStringFromArray = array => {
  let outputString = "";

  array.forEach((item, i) => {
    if (item === "(") {
      outputString += "(";
    } else if (item === ")") {
      if (array[i + 1] && array[i + 1] === ")") {
        outputString += ")";
      } else {
        outputString += ") ";
      }
    } else {
      let rightWhiteSpace = " ";
      if (i > 0 && array[i + 1] === ")") {
        rightWhiteSpace = "";
      }
      outputString += `${item}${rightWhiteSpace}`;
    }
  });

  return `${outputString.trim()}`;
};

export const addParenthesisForSubSentence = subSentence => [
  "(",
  ...subSentence,
  ")"
];

export const getCorrectSubstringOfNegation = (words, index) => {
  if (words[index + 1] === "(") {
    const restOfSentence = words.slice(index);
    const indexOfClosingParenthesis = getIndexOfClosingParenthesis(
      restOfSentence
    );
    return restOfSentence.slice(0, indexOfClosingParenthesis + 1);
  } else {
    return words.slice(index, index + 2);
  }
};

export const getCorrectSubstringOfConnector = (words, index) => {
  if (words[index + 1] === "(") {
    const restOfSentence = words.slice(index);
    const indexOfClosingParenthesis = getIndexOfClosingParenthesis(
      restOfSentence
    );
    return [
      ...words.slice(index - 1, index),
      ...restOfSentence.slice(0, indexOfClosingParenthesis + 1)
    ];
  } else {
    return words.slice(index - 1, index + 2);
  }
};

const getIndexOfClosingParenthesis = restOfSentence => {
  const numOfSignificantParenthesis = getNumOfSignificantParenthesis(
    restOfSentence
  );
  let indexOfClosingParenthesis = 0;
  let numOfFoundClosingParenthesis = 0;

  restOfSentence.forEach((word, index) => {
    if (indexOfClosingParenthesis !== 0) {
      return;
    }
    if (word === ")") {
      numOfFoundClosingParenthesis++;
    }
    if (
      numOfFoundClosingParenthesis > 0 &&
      numOfFoundClosingParenthesis === numOfSignificantParenthesis
    ) {
      indexOfClosingParenthesis = index;
    }
  });

  return indexOfClosingParenthesis;
};

const getNumOfSignificantParenthesis = words => {
  let numOfLeftParenthesis = 0,
    numOfRightParenthesis = 0;

  words.forEach(word => {
    if (word === ")") {
      numOfRightParenthesis++;
    }
    if (
      numOfLeftParenthesis > 0 &&
      numOfRightParenthesis === numOfLeftParenthesis
    ) {
      return;
    }
    if (word === "(") {
      numOfLeftParenthesis++;
    }
  });

  return numOfLeftParenthesis;
};
