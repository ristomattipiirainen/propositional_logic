/**
 * Source: https://fi.wikipedia.org/wiki/Propositiologiikka
 *
 * A B !A !B  A&&B  A||B   A–>B   A<–>B
 * 1 1 0  0   1     1      1      1
 * 1 0 0  1   0     1      0      0
 * 0 1 1  0   0     1      1      0
 * 0 0 1  1   0     0      1      1
 *
 * Premise 1: If it's raining then it's cloudy.
 * Premise 2: It's raining.
 * Conclusion: It's cloudy.
 *
 * */

export const negation = proposition => !proposition;

export const conjunction = (a, b) => a && b;

export const disjunction = (a, b) => a || b;

export const implication = (a, b) => !(a && !b);

export const equivalence = (a, b) => (a && b) || (!a && !b);

export const xor = (a, b) => (a || b) && !(a && b);
