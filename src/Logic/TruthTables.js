/**
 * Source: https://fi.wikipedia.org/wiki/Propositiologiikka
 *
 * A B !A !B  A&&B  A||B   A–>B   A<–>B
 * 1 1 0  0   1     1      1      1
 * 1 0 0  1   0     1      0      0
 * 0 1 1  0   0     1      1      0
 * 0 0 1  1   0     0      1      1
 *
 */

import {
  conjunction,
  disjunction,
  equivalence,
  implication
} from "./PropositionalLogic";

export const formTruthTable = (propositionsToUse, sentence = null) => {
  let table = generateBasicValuesToTable(propositionsToUse);

  if (sentence) {
    sentence.forEach(proposition => {
      const sentenceToBeAdded = getSentenceToBeAdded(proposition, table);
      table.push(sentenceToBeAdded);
    });
  }

  return table;
};

export const generateBasicValuesToTable = propositionsToUse => {
  const rowsInTable = getNumOfRows(propositionsToUse.length);

  return propositionsToUse.map((proposition, i) => {
    return addItemToRow(proposition, i, rowsInTable);
  });
};

export const getSentenceToBeAdded = (sentence, table) => {
  let newSentence;
  if (sentence && sentence.includes("!")) {
    const key = sentence.charAt(1);
    newSentence = getValuesForNegation(table, key);
  }
  return { [sentence]: newSentence };
};

export const getValuesForNegation = (table, key) => {
  const column = table.find(values => Object.keys(values)[0] === key);
  return column[key].map(value => (value === 1 ? 0 : 1));
};

const getColumns = (table, key) => {
  return {
    columnOne: table.find(values => Object.keys(values)[0] === key.charAt(0)),
    columnTwo: table.find(values => Object.keys(values)[0] === key.charAt(1))
  };
};

const getValuesForConnector = (connector, columnOne, columnTwo) => {
  return Object.values(columnOne)[0].map((value, i) => {
    return connector(value, Object.values(columnTwo)[0][i]) ? 1 : 0;
  });
};

export const getValuesForConjunction = (table, key) => {
  const { columnOne, columnTwo } = getColumns(table, key);

  return getValuesForConnector(conjunction, columnOne, columnTwo);
};

export const getValuesForDisjunction = (table, key) => {
  const { columnOne, columnTwo } = getColumns(table, key);

  return getValuesForConnector(disjunction, columnOne, columnTwo);
};

export const getValuesForImplication = (table, key) => {
  const { columnOne, columnTwo } = getColumns(table, key);

  return getValuesForConnector(implication, columnOne, columnTwo);
};

export const getValuesForEquivalence = (table, key) => {
  const { columnOne, columnTwo } = getColumns(table, key);

  return getValuesForConnector(equivalence, columnOne, columnTwo);
};

export const getNumOfRows = numOfRows => 2 ** numOfRows;

export const addItemToRow = (proposition, iteration, rowsInTable) => {
  let row = [];
  const frequency = rowsInTable / 2 ** (iteration + 1);

  let valueToEnter = 1;
  let numOfValuesToAdd = rowsInTable;

  while (numOfValuesToAdd > 0) {
    let valuesToAdd = [];
    for (let i = 0; i < frequency; i++) {
      valuesToAdd.push(valueToEnter);
    }
    row.push(...valuesToAdd);
    valueToEnter = valueToEnter === 1 ? 0 : 1;
    numOfValuesToAdd = numOfValuesToAdd - frequency;
  }

  return { [proposition]: row };
};
