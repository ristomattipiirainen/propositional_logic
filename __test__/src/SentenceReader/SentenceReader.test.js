import {
  getClausesFromImplication,
  getPropositionsOfConjunction,
  getTruthValue,
  hasConjunctions,
  isImplication,
  isNegation,
  normalizeClause
} from "../../../src/WrittenLanguage/SentenceReader";

describe("WrittenLanguage", () => {
  test("normalizes clauses", () => {
    expect(normalizeClause(`don't be a dick`)).toEqual("do not be a dick");
    expect(normalizeClause(`this ain't gonna hurt`)).toEqual(
      "this is not going to hurt"
    );
    expect(normalizeClause(`I'll be missing you`)).toEqual(
      "I will be missing you"
    );
  });

  test("gets truth value", () => {
    expect(getTruthValue("i am not a monkey")).toBeFalsy();
    expect(getTruthValue("coffee is not good")).toBeFalsy();

    expect(getTruthValue("i am a monkey")).toBeTruthy();
    expect(getTruthValue("beer tastes very yummy")).toBeTruthy();
  });

  test("finds implications", () => {
    expect(isImplication("if it rains, then i will be wet")).toBeTruthy();
    expect(isImplication("if i run, then i run")).toBeTruthy();
    expect(isImplication("If i run, THEN i run")).toBeTruthy();

    expect(isImplication("ifhehethen")).toBeFalsy();
  });

  test("finds negations", () => {
    expect(isNegation(`i don't go to school`)).toBeTruthy();
    expect(isNegation(`sweden is not in america`)).toBeTruthy();

    expect(isNegation(`i walk to school`)).toBeFalsy();
    expect(isNegation(`mark wahlberg is an actor`)).toBeFalsy();
  });

  test("finds conjunctions", () => {
    expect(hasConjunctions("I write an exam and I cheat")).toBeTruthy();
    expect(hasConjunctions("foo and bar")).toBeTruthy();

    expect(hasConjunctions("I drive car or take a bus")).toBeFalsy();
    expect(hasConjunctions("and")).toBeFalsy();
    expect(hasConjunctions(" and foo foo")).toBeFalsy();
    expect(hasConjunctions("foo foo and ")).toBeFalsy();
  });

  test("gets propositions of conjunction", () => {
    let propositionOne = "It rains";
    let propositionTwo = "I am wet";
    expect(
      getPropositionsOfConjunction(propositionOne + " and " + propositionTwo)
    ).toEqual([propositionOne, propositionTwo]);

    propositionOne = "Roses are red";
    propositionTwo = "skies are blue";
    expect(
      getPropositionsOfConjunction(propositionOne + " and " + propositionTwo)
    ).toEqual([propositionOne, propositionTwo]);
  });

  test("separates implication to two different clauses", () => {
    const implication =
      "If I write an exam and I cheat then i will get caught and i will fail";
    const expected = [
      "I write an exam and I cheat",
      "i will get caught and i will fail"
    ];
    expect(getClausesFromImplication(implication)).toEqual(expected);
  });
});
