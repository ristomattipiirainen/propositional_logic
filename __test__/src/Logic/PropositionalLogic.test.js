import {
  conjunction,
  disjunction,
  equivalence,
  implication,
  negation,
  xor
} from "../../../src/Logic/PropositionalLogic";

describe("PropositionalLogic", () => {
  test("finds negations", () => {
    expect(negation(0)).toBeTruthy();
    expect(negation(1)).toBeFalsy();
  });

  test("conjunctions", () => {
    expect(conjunction(1, 1)).toBeTruthy();

    expect(conjunction(0, 0)).toBeFalsy();
    expect(conjunction(0, 1)).toBeFalsy();
    expect(conjunction(1, 0)).toBeFalsy();
  });

  test("disjunctions", () => {
    expect(disjunction(1, 1)).toBeTruthy();
    expect(disjunction(0, 1)).toBeTruthy();
    expect(disjunction(1, 0)).toBeTruthy();

    expect(disjunction(0, 0)).toBeFalsy();
  });

  test("implications", () => {
    expect(implication(1, 1)).toBeTruthy();
    expect(implication(0, 1)).toBeTruthy();
    expect(implication(0, 0)).toBeTruthy();

    expect(implication(1, 0)).toBeFalsy();
  });

  test("equivalences", () => {
    expect(equivalence(1, 1)).toBeTruthy();
    expect(equivalence(0, 0)).toBeTruthy();

    expect(equivalence(0, 1)).toBeFalsy();
    expect(equivalence(1, 0)).toBeFalsy();
  });

  test("xor", () => {
    expect(xor(1, 0)).toBeTruthy();
    expect(xor(0, 1)).toBeTruthy();

    expect(xor(0, 0)).toBeFalsy();
    expect(xor(1, 1)).toBeFalsy();
  });
});
