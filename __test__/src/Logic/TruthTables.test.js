import {
  addItemToRow,
  formTruthTable,
  generateBasicValuesToTable,
  getNumOfRows,
  getSentenceToBeAdded,
  getValuesForConjunction,
  getValuesForDisjunction,
  getValuesForEquivalence,
  getValuesForImplication,
  getValuesForNegation
} from "../../../src/Logic/TruthTables";

describe("TruthTables", () => {
  test("generates basic propositions - p, q", () => {
    const result = generateBasicValuesToTable(["p", "q"]);

    const expectedResult = [{ p: [1, 1, 0, 0] }, { q: [1, 0, 1, 0] }];
    expect(result).toEqual(expectedResult);
  });

  test("generates basic propositions - p, q, r", () => {
    const result = generateBasicValuesToTable(["p", "q", "r"]);

    const expectedResult = [
      { p: [1, 1, 1, 1, 0, 0, 0, 0] },
      { q: [1, 1, 0, 0, 1, 1, 0, 0] },
      { r: [1, 0, 1, 0, 1, 0, 1, 0] }
    ];
    expect(result).toEqual(expectedResult);
  });

  test("form truth table - p, q, r & sentence !q", () => {
    const result = formTruthTable(["p", "q", "r"], ["!q"]);

    const expectedResult = [
      { p: [1, 1, 1, 1, 0, 0, 0, 0] },
      { q: [1, 1, 0, 0, 1, 1, 0, 0] },
      { r: [1, 0, 1, 0, 1, 0, 1, 0] },
      { "!q": [0, 0, 1, 1, 0, 0, 1, 1] }
    ];
    expect(result).toEqual(expectedResult);
  });

  test("gets num of rows in table", () => {
    expect(getNumOfRows(1)).toEqual(2);
    expect(getNumOfRows(2)).toEqual(4);
    expect(getNumOfRows(3)).toEqual(8);
    expect(getNumOfRows(4)).toEqual(16);
  });

  test("gets values for negation", () => {
    const rows = [
      { p: [1, 1, 1, 1, 0, 0, 0, 0] },
      { q: [1, 1, 0, 0, 1, 1, 0, 0] },
      { r: [1, 0, 1, 0, 1, 0, 1, 0] }
    ];

    let expectedResult = [0, 0, 1, 1, 0, 0, 1, 1];

    let result = getValuesForNegation(rows, "q");
    expect(result).toEqual(expectedResult);

    expectedResult = [0, 1, 0, 1, 0, 1, 0, 1];

    result = getValuesForNegation(rows, "r");
    expect(result).toEqual(expectedResult);

    expectedResult = [0, 0, 0, 0, 1, 1, 1, 1];

    result = getValuesForNegation(rows, "p");
    expect(result).toEqual(expectedResult);
  });

  test("gets values for conjunction", () => {
    const rows = [
      { p: [1, 1, 1, 1, 0, 0, 0, 0] },
      { q: [1, 1, 0, 0, 1, 1, 0, 0] },
      { r: [1, 0, 1, 0, 1, 0, 1, 0] }
    ];

    let expectedResult = [1, 1, 0, 0, 0, 0, 0, 0];

    let result = getValuesForConjunction(rows, "pq");
    expect(result).toEqual(expectedResult);

    expectedResult = [1, 0, 0, 0, 1, 0, 0, 0];

    result = getValuesForConjunction(rows, "qr");
    expect(result).toEqual(expectedResult);

    expectedResult = [1, 0, 1, 0, 0, 0, 0, 0];

    result = getValuesForConjunction(rows, "pr");
    expect(result).toEqual(expectedResult);
  });

  test("gets values for disjunction", () => {
    const rows = [
      { p: [1, 1, 1, 1, 0, 0, 0, 0] },
      { q: [1, 1, 0, 0, 1, 1, 0, 0] },
      { r: [1, 0, 1, 0, 1, 0, 1, 0] }
    ];

    let expectedResult = [1, 1, 1, 1, 1, 1, 0, 0];

    let result = getValuesForDisjunction(rows, "pq");
    expect(result).toEqual(expectedResult);

    expectedResult = [1, 1, 1, 0, 1, 1, 1, 0];

    result = getValuesForDisjunction(rows, "qr");
    expect(result).toEqual(expectedResult);

    expectedResult = [1, 1, 1, 1, 1, 0, 1, 0];

    result = getValuesForDisjunction(rows, "pr");
    expect(result).toEqual(expectedResult);
  });

  test("gets values for implications", () => {
    const rows = [
      { p: [1, 1, 1, 1, 0, 0, 0, 0] },
      { q: [1, 1, 0, 0, 1, 1, 0, 0] },
      { r: [1, 0, 1, 0, 1, 0, 1, 0] }
    ];

    let expectedResult = [1, 1, 0, 0, 1, 1, 1, 1];

    let result = getValuesForImplication(rows, "pq");
    expect(result).toEqual(expectedResult);

    expectedResult = [1, 0, 1, 1, 1, 0, 1, 1];

    result = getValuesForImplication(rows, "qr");
    expect(result).toEqual(expectedResult);

    expectedResult = [1, 0, 1, 0, 1, 1, 1, 1];

    result = getValuesForImplication(rows, "pr");
    expect(result).toEqual(expectedResult);
  });

  test("gets values for equivalences", () => {
    const rows = [
      { p: [1, 1, 1, 1, 0, 0, 0, 0] },
      { q: [1, 1, 0, 0, 1, 1, 0, 0] },
      { r: [1, 0, 1, 0, 1, 0, 1, 0] }
    ];

    let expectedResult = [1, 1, 0, 0, 0, 0, 1, 1];

    let result = getValuesForEquivalence(rows, "pq");
    expect(result).toEqual(expectedResult);

    expectedResult = [1, 0, 0, 1, 1, 0, 0, 1];

    result = getValuesForEquivalence(rows, "qr");
    expect(result).toEqual(expectedResult);

    expectedResult = [1, 0, 1, 0, 0, 1, 0, 1];

    result = getValuesForEquivalence(rows, "pr");
    expect(result).toEqual(expectedResult);
  });

  test("adds negation to table", () => {
    const rows = [
      { p: [1, 1, 1, 1, 0, 0, 0, 0] },
      { q: [1, 1, 0, 0, 1, 1, 0, 0] },
      { r: [1, 0, 1, 0, 1, 0, 1, 0] }
    ];

    const expectedResult = { "!q": [0, 0, 1, 1, 0, 0, 1, 1] };

    const result = getSentenceToBeAdded("!q", rows);
    expect(result).toEqual(expectedResult);
  });

  test("adds item to row", () => {
    expect(addItemToRow("p", 0, 4)).toEqual({ p: [1, 1, 0, 0] });
    expect(addItemToRow("p", 0, 8)).toEqual({ p: [1, 1, 1, 1, 0, 0, 0, 0] });
    expect(addItemToRow("p", 0, 16)).toEqual({
      p: [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    });

    expect(addItemToRow("q", 1, 4)).toEqual({ q: [1, 0, 1, 0] });
    expect(addItemToRow("q", 1, 8)).toEqual({ q: [1, 1, 0, 0, 1, 1, 0, 0] });
    expect(addItemToRow("q", 1, 16)).toEqual({
      q: [1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0]
    });

    expect(addItemToRow("r", 2, 8)).toEqual({ r: [1, 0, 1, 0, 1, 0, 1, 0] });
    expect(addItemToRow("r", 2, 16)).toEqual({
      r: [1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0]
    });

    expect(addItemToRow("p", 0, 16)).toEqual({
      p: [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    });
    expect(addItemToRow("q", 1, 16)).toEqual({
      q: [1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0]
    });
    expect(addItemToRow("r", 2, 16)).toEqual({
      r: [1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0]
    });
    expect(addItemToRow("s", 3, 16)).toEqual({
      s: [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0]
    });

    expect(addItemToRow("p", 0, 32)).toEqual({
      p: [
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    });
    expect(addItemToRow("q", 1, 32)).toEqual({
      q: [
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ]
    });
    expect(addItemToRow("r", 2, 32)).toEqual({
      r: [
        1,
        1,
        1,
        1,
        0,
        0,
        0,
        0,
        1,
        1,
        1,
        1,
        0,
        0,
        0,
        0,
        1,
        1,
        1,
        1,
        0,
        0,
        0,
        0,
        1,
        1,
        1,
        1,
        0,
        0,
        0,
        0
      ]
    });
    expect(addItemToRow("s", 3, 32)).toEqual({
      s: [
        1,
        1,
        0,
        0,
        1,
        1,
        0,
        0,
        1,
        1,
        0,
        0,
        1,
        1,
        0,
        0,
        1,
        1,
        0,
        0,
        1,
        1,
        0,
        0,
        1,
        1,
        0,
        0,
        1,
        1,
        0,
        0
      ]
    });
    expect(addItemToRow("t", 4, 32)).toEqual({
      t: [
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0,
        1,
        0
      ]
    });
  });
});
