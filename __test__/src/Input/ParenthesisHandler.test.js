import {
  addParenthesis,
  addParenthesisForSubSentence,
  formStringFromArray,
  getAllConnectors,
  getCorrectSubstringOfConnector,
  getCorrectSubstringOfNegation,
  getNumOfConnectors,
  hasRemainingDisjunctions
} from "../../../src/Input/ParenthesisHandler";

describe("Parenthesis Handler", () => {
  test("adds parenthesises to conjunctions", () => {
    expect(addParenthesis("p AND q")).toEqual("(p AND q)");
    expect(addParenthesis("p AND q AND r")).toEqual("(p AND (q AND r))");
    expect(addParenthesis("p AND q AND r AND s")).toEqual(
      "(p AND (q AND (r AND s)))"
    );
    expect(addParenthesis("p AND q AND r AND s")).toEqual(
      "(p AND (q AND (r AND s)))"
    );
    expect(addParenthesis("p AND q AND r AND s AND t")).toEqual(
      "(p AND (q AND (r AND (s AND t))))"
    );
    expect(addParenthesis("p AND q AND r AND s AND t AND u")).toEqual(
      "(p AND (q AND (r AND (s AND (t AND u)))))"
    );
    expect(addParenthesis("p AND q AND r AND s AND t AND u AND v")).toEqual(
      "(p AND (q AND (r AND (s AND (t AND (u AND v))))))"
    );
  });

  test("adds parenthesises to disjunctions", () => {
    expect(addParenthesis("p OR q")).toEqual("(p OR q)");
    expect(addParenthesis("p OR q OR r")).toEqual("(p OR (q OR r))");
    expect(addParenthesis("p OR q OR r OR s")).toEqual(
      "(p OR (q OR (r OR s)))"
    );
    expect(addParenthesis("p OR q OR r OR s OR t")).toEqual(
      "(p OR (q OR (r OR (s OR t))))"
    );
  });

  test("mixed disjunctions and conjunctions", () => {
    expect(addParenthesis("p OR q AND r")).toEqual("(p OR (q AND r))");
    expect(addParenthesis("p OR q OR r AND s")).toEqual(
      "(p OR (q OR (r AND s)))"
    );
    expect(addParenthesis("p OR q AND r OR s")).toEqual(
      "(p OR ((q AND r) OR s))"
    );
    expect(addParenthesis("p OR q AND r OR s AND t")).toEqual(
      "(p OR ((q AND r) OR (s AND t)))"
    );
    expect(addParenthesis("p OR q AND r OR s AND t OR u")).toEqual(
      "(p OR ((q AND r) OR ((s AND t) OR u)))"
    );
  });

  test("gets num of connectors", () => {
    expect(getNumOfConnectors("p AND q")).toEqual(1);
    expect(getNumOfConnectors("p AND q OR r")).toEqual(2);
    expect(getNumOfConnectors("p AND q AND s AND q")).toEqual(3);
    expect(getNumOfConnectors("p XoR q OR s -> t <=> x OR s")).toEqual(5);
    expect(getNumOfConnectors("p -> q <=> r XoR s OR t AND u -> v")).toEqual(6);
    expect(
      getNumOfConnectors(
        "p -> q <=> r XoR s OR t AND u -> v AND p -> q <=> r XoR s OR t AND u -> v"
      )
    ).toEqual(13);
    expect(
      getNumOfConnectors(
        "p -> q <=> r XoR s OR t AND u -> v AND p -> q <=> r XoR s OR t AND u -> v OR p -> q <=> r XoR s OR t AND u -> v AND p -> q <=> r XoR s OR t AND u -> v"
      )
    ).toEqual(27);
  });

  test("forms a correct string out of propositions and connectors", () => {
    let sentenceAsArray = ["(", "a", "AND", "(", "b", "AND", "c", ")", ")"];

    expect(formStringFromArray(sentenceAsArray)).toEqual("(a AND (b AND c))");

    sentenceAsArray = [
      "(",
      "a",
      "AND",
      "(",
      "b",
      "AND",
      "c",
      ")",
      "OR",
      "(",
      "d",
      "AND",
      "e",
      ")",
      ")"
    ];

    expect(formStringFromArray(sentenceAsArray)).toEqual(
      "(a AND (b AND c) OR (d AND e))"
    );
  });

  test("gets all connectors", () => {
    let sentence = "p AND q AND s AND q";

    expect(getAllConnectors(sentence)).toEqual(["AND", "AND", "AND"]);

    sentence = "p AND q OR s -> q <=> r XoR s";

    expect(getAllConnectors(sentence)).toEqual([
      "AND",
      "OR",
      "->",
      "<=>",
      "XoR"
    ]);
  });

  test("gets remaining connectors", () => {
    let sentence = "p AND q AND s AND q";

    expect(getAllConnectors(sentence)).toEqual(["AND", "AND", "AND"]);

    sentence = "p AND q OR s -> q <=> r XoR s";

    expect(getAllConnectors(sentence)).toEqual([
      "AND",
      "OR",
      "->",
      "<=>",
      "XoR"
    ]);
  });

  test("has remaining disjunctions", () => {
    const connectors = ["AND", "AND", "AND", "OR", "AND"];

    expect(hasRemainingDisjunctions(connectors, 6)).toBeFalsy();
    expect(hasRemainingDisjunctions(connectors, 4)).toBeFalsy();

    expect(hasRemainingDisjunctions(connectors, 0)).toBeTruthy();
    expect(hasRemainingDisjunctions(connectors, 1)).toBeTruthy();
    expect(hasRemainingDisjunctions(connectors, 2)).toBeTruthy();
    expect(hasRemainingDisjunctions(connectors, 3)).toBeTruthy();
  });

  test("add parenthesis for subsentence", () => {
    const negation = ["!", "(", "p", "AND", "q", ")"];
    expect(addParenthesisForSubSentence(negation)).toEqual([
      "(",
      "!",
      "(",
      "p",
      "AND",
      "q",
      ")",
      ")"
    ]);

    const subSentence = ["p", "AND", "q"];
    expect(addParenthesisForSubSentence(subSentence)).toEqual([
      "(",
      "p",
      "AND",
      "q",
      ")"
    ]);
  });

  test("gets correct substring of negation", () => {
    let words = ["p", "OR", "q", "AND", "!", "r", "->", "s"];
    expect(getCorrectSubstringOfNegation(words, 4)).toEqual(["!", "r"]);

    words = ["p", "OR", "q", "AND", "!", "(", "r", "->", "s", ")"];
    expect(getCorrectSubstringOfNegation(words, 4)).toEqual([
      "!",
      "(",
      "r",
      "->",
      "s",
      ")"
    ]);

    words = ["q", "AND", "!", "(", "r", "->", "(", "!", "s", ")", ")"];
    expect(getCorrectSubstringOfNegation(words, 2)).toEqual([
      "!",
      "(",
      "r",
      "->",
      "(",
      "!",
      "s",
      ")",
      ")"
    ]);

    words = [
      "p",
      "AND",
      "q",
      "AND",
      "!",
      "(",
      "(",
      "p",
      "AND",
      "q",
      ")",
      "AND",
      "(",
      "r",
      "AND",
      "s",
      ")",
      ")",
      "AND",
      "!",
      "r"
    ];
    expect(getCorrectSubstringOfNegation(words, 4)).toEqual([
      "!",
      "(",
      "(",
      "p",
      "AND",
      "q",
      ")",
      "AND",
      "(",
      "r",
      "AND",
      "s",
      ")",
      ")"
    ]);
  });

  test("gets correct substring of connector", () => {
    let words = ["p", "OR", "q", "AND", "!", "r", "->", "s"];
    expect(getCorrectSubstringOfConnector(words, 1)).toEqual(["p", "OR", "q"]);

    words = ["p", "OR", "(", "p", "AND", "q", ")", "->", "r"];
    expect(getCorrectSubstringOfConnector(words, 1)).toEqual([
      "p",
      "OR",
      "(",
      "p",
      "AND",
      "q",
      ")"
    ]);

    words = [
      "p",
      "AND",
      "q",
      "OR",
      "(",
      "(",
      "r",
      "AND",
      "s",
      ")",
      "->",
      "t",
      ")",
      "AND",
      "(",
      "p",
      "AND",
      "q"
    ];
    expect(getCorrectSubstringOfConnector(words, 3)).toEqual([
      "q",
      "OR",
      "(",
      "(",
      "r",
      "AND",
      "s",
      ")",
      "->",
      "t",
      ")"
    ]);
  });
});
